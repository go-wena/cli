module gitee.com/go-wena/cli

go 1.16

require (
	gitee.com/go-errors/errors v0.0.1
	github.com/segmentio/go-snakecase v1.2.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/pflag v1.0.5
)
