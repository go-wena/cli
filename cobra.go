package cli

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

type (
	Command            struct{ cobra.Command }
	FlagSet            = pflag.FlagSet
	ShellCompDirective = cobra.ShellCompDirective
)

const (
	ShellCompDirectiveNoFileComp = cobra.ShellCompDirectiveNoFileComp
)

// CheckErr prints the msg with the prefix 'Error:' and exits with error code 1. If the msg is nil, it does nothing.
func CheckErr(msg interface{}) {
	if msg != nil {
		_, _ = fmt.Fprintln(os.Stderr, "Error:", msg)
		os.Exit(1)
	}
}

type PositionalArgs = cobra.PositionalArgs

func ExactValidArgs(n int) Option {
	return OptionApply(func(c *Command) { c.Args = cobra.ExactValidArgs(n) })
}

func ExactArgs(n int) Option {
	return OptionApply(func(c *Command) { c.Args = cobra.ExactArgs(n) })
}
